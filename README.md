# Strava-app ver 1.7

This is simple python program that allows user to view his data from strava.com service.

**Requirements:**
- installed python ver. > 3.8
- installed all libraries from requirements.txt 
```bash
pip install -r requirements. txt
```

### Tutorial for creating a strava api app
https://jessicasalbert.medium.com/holding-your-hand-through-stravas-api-e642d15695f2
### Links from tutorial:
#### Asigning scope to your strava app:
https://www.strava.com/oauth/authorize?client_id=[YOUR_CLIENT_ID]&response_type=code&redirect_uri=http://localhost/exchange_token&approval_prompt=force&scope=activity:read_all,activity:read,read_all
#### Increasing application rights according to scope::
https://www.strava.com/oauth/token?client_id=[YOUR_CLIENT_ID]&client_secret=[YOUR_CLIENT_SECRET]&code=[RECIEVED_CODE]&grant_code=authotization_code
### Creating credentials.json file
Use this command to create files with necesary credentials from: https://www.strava.com/settings/api.
```bash
cat > credentials1.json << "EOF" 
{
  "client_id": "[CLIENT_ID]",
  "client_secret": "[CLIENT_SECRET]",
  "refresh_token": "[REFRESH_TOKEN]"
}
EOF
```
## Running strava app:
```bash
python strava-app.py
```

## Available commands:

| Command       | Description            | Suported since version |
|:--------------|------------------------|-----------------------:|
| activities    | Show user activities   |                    1.0 |
| activity      | Show activity details  |                    1.2 |
| comments      | Show activity comments |                    1.3 |
| kudos         | Show activity kudoers  |                    1.4 |
| athlete       | Show athlete details   |                    1.5 |
| athlete-stats | Show athlete statistic |                    1.6 |
| athlete-clubs | Show athlete clubs     |                    1.7 |

### Command details:
- `activites`

**Example uses:**
```bash
python strava-app.py activities --help
python strava-app.py activites --activity_type="Walk" --all_activities=False --statistics=True
```
    
- `activity`

**Example uses:**
```bash
python strava-app.py activity --help
python strava-app.py activity --activity_id 12345678901
```

- `comments`

**Example uses:**
```bash
python strava-app.py comments --help
python strava-app.py comments --activity_id 12345678901
```

- `kudos`

**Example uses:**
```bash
python strava-app.py kudos --help
python strava-app.py kudos --activity_id 12345678901
```

- `athlete`

**Example uses:**
```bash
python strava-app.py athlete --help
python strava-app.py athlete
```

- `athlete-stats`

**Example uses:**
```bash
python strava-app.py athlete-stats --help
python strava-app.py athlete-stats --athlete_id 12345678
```

- `athlete-clubs`

**Example uses:**
```bash
python strava-app.py athlete-clubs --help
python strava-app.py athlete-clubs
```