import click
from src.activities import activity_details, activity_comments, activity_kudos
from src.athlete import athlete_activities, activ_stats, ath_details, ath_statistics, ath_clubs
from src.table_formater import activities_table, stats_tabe, activity_table, comments_table, kudos_table


@click.group("strava")
def strava():
    """
    Simple program that displays user related data from strava.com service.
    """
    pass


@click.command()
@click.option("--activity_type", default="Run", help="Filter results by activity type.")
@click.option("--all_activities", default=False, help="If true all athlete activites will be considered else only "
                                                      "last 30.")
@click.option("--statistics", default=False, help="Shows a bunch of statstic from requested data set.")
def activities(activity_type, all_activities, statistics):
    """
    Show user activities.

    :param activity_type: Allowed ativity type: AlpineSki, BackcountrySki, Canoeing, Crossfit,
    EBikeRide, Elliptical, Golf, Handcycle, Hike, IceSkate, InlineSkate, Kayaking, Kitesurf, NordicSki, Ride,
    RockClimbing, RollerSki, Rowing, Run, Sail, Skateboard, Snowboard, Snowshoe, Soccer, StairStepper,
    StandUpPaddling, Surfing, Swim, Velomobile, VirtualRide, VirtualRun, Walk, WeightTraining, Wheelchair, Windsurf,
    Workout, Yoga, by defualt is set to "Run"
    :param all_activities: if True: all athlete activities else: 30 last activites, by default = False
    :param statistics: Shows a bunch of statstic from requested data set.
    """
    activites = athlete_activities(activity=activity_type, all_activities=all_activities)
    activities_table(data=activites)

    if statistics:
        activites_stats = activ_stats(data_set=activites)
        stats_tabe(data=activites_stats)


@click.command()
@click.option("--activity_id", required=1, help="Required activity id")
def activity(activity_id):
    """
    Show activity details (name, distance(m), moving time(s), elevation gain(m), type, time start, country, number of
    kudos, number of comments, calories(kcal), device)
    If no activity was found, respective message is displayed.
    """
    act = activity_details(activity_id=activity_id)
    activity_table(act)


@click.command()
@click.option("--activity_id", required=1, help="Required activity id")
def comments(activity_id):
    """
    Show activity comments (comment text, comment author).
    If no comments were found for activity, respective message is displayed.
    """
    activ_comments = activity_comments(activity_id=activity_id)
    comments_table(activ_comments)


@click.command()
@click.option("--activity_id", required=1, help="Required activity id")
def kudos(activity_id):
    """
    Show activity kudos/kudoers (kudoer).
    If no kudos were found for activity, respective message is displayed.
    """
    activ_kudos = activity_kudos(activity_id=activity_id)
    kudos_table(activ_kudos)


@click.command()
def athlete():
    """
    Show information about athlete/user.
    """
    athlete_info = ath_details()
    activity_table(athlete_info)


@click.command()
@click.option("--athlete_id", required=1, help="Required athlete id")
def athlete_stats(athlete_id):
    """
    Show athlete statistics.
    """
    stats = ath_statistics(athlate_id=athlete_id)
    activity_table(stats)


@click.command()
def athlete_clubs():
    """
    Show clubs whose membership includes the authenticated athlete.
    If no clubs were found for athlete, respective message is displayed.
    """
    clubs = ath_clubs()
    activity_table(clubs)


strava.add_command(activities)
strava.add_command(activity)
strava.add_command(comments)
strava.add_command(kudos)
strava.add_command(athlete)
strava.add_command(athlete_stats)
strava.add_command(athlete_clubs)

if __name__ == '__main__':
    strava()
