from datetime import timedelta
from src.methods.api_athlete import Athlete

athlete = Athlete()


def get_athlete_activities(*, items=200, _all=True) -> list:
    if _all:
        page = 1
        athlete_all_activities = list()
        while True:
            activities = athlete.activities(page_items=items, page_index=page)
            if len(activities) > 0:
                athlete_all_activities.extend(activities)
            else:
                break
            page += 1
        return athlete_all_activities
    return athlete.activities()


def max_value(data_set: list, attribute: str):
    return max(data_set, key=lambda x: x[attribute])


def athlete_activities(*, activity="Run", all_activities=False) -> list:
    """
    :param all_activities: if True: all athlete activities else: 30 last activites
    :param activity: Allowed cativity type: AlpineSki, BackcountrySki, Canoeing, Crossfit,
    EBikeRide, Elliptical, Golf, Handcycle, Hike, IceSkate, InlineSkate, Kayaking, Kitesurf, NordicSki, Ride,
    RockClimbing, RollerSki, Rowing, Run, Sail, Skateboard, Snowboard, Snowshoe, Soccer, StairStepper,
    StandUpPaddling, Surfing, Swim, Velomobile, VirtualRide, VirtualRun, Walk, WeightTraining, Wheelchair, Windsurf,
    Workout, Yoga
    :return: list
    """
    fileds = ["type", "id", "distance", "moving_time", "start_date_local", "average_speed", "max_speed"]
    data_set = get_athlete_activities(items=200, _all=all_activities)
    filtered_activities = [{k: v for k, v in _.items() if k in fileds} for _ in data_set if _["type"] == activity]
    return filtered_activities


def activ_stats(data_set: list) -> list:
    statistics = [("Number of activities", len(data_set)),
                  ("Longest distance", round(max(data_set, key=lambda x: x['distance'])['distance'] / 1000, 2)),
                  ("Longest duration", str(timedelta(seconds=max(data_set, key=lambda x: x['moving_time'])
                  ['moving_time']))),
                  ("Top average speed", round(max_value(data_set=data_set, attribute='average_speed')['average_speed']
                                              * 3.6, 2))
                  ]
    return statistics


def ath_details() -> list:
    not_allowed = ["resource_state", "state", "premium", "summit", "created_at", "updated_at", "badge_type_id",
                   "profile_medium", "profile", "friend", "follower", "bio"]
    athlete_info = athlete.athlete()
    filtered_athlete_info = [[str(key).replace("_", " ").capitalize(), athlete_info[key]] for key in athlete_info if
                             key not in not_allowed]
    return filtered_athlete_info


def ath_statistics(athlate_id: str):
    stats = athlete.stats(athlete_id=athlate_id)
    optimized_stats = [[str(key).replace("_", " ").capitalize(), stats[key]["count"]] if isinstance(stats[key], dict)
                       else [str(key).replace("_", " ").capitalize(), stats[key]]
                       for key in stats if stats[key]]
    return optimized_stats


def ath_clubs():
    clubs = athlete.clubs()
    required = ["name", ]
    if not clubs:
        return f"Athlete does't belongs to any club."
    filtered_data = [[club[key]] for club in clubs for key in club if key in required]
    return filtered_data
