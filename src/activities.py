from src.methods.api_activities import Activities

activ = Activities()


def activity_details(activity_id):
    activity = activ.activity(activity_id=activity_id)
    if "errors" in activity:
        return f"Invalid activity id: {activity_id}."

    data = {k: activity[k] for k, v in activity.items() if not isinstance(v, (dict, list))}
    property_filter = ["name", "distance", "moving_time", "total_elevation_gain", "calories", "type",
                       "start_date_local", "location_country", "kudos_count", "comment_count", "device_name"]

    filtered_data = [[str(k).capitalize().replace("_", " "), v] for k, v in data.items() if k in property_filter]
    return filtered_data


def activity_comments(activity_id):
    comments = activ.activity_comments(activity_id=activity_id)
    if "errors" in comments:
        return f"Invalid activity id: {activity_id}."
    if not comments:
        return f"No comments found for activity: {activity_id}."
    allowed = ["text", "athlete"]
    filtered_comments = [
        [comment[k] if isinstance(comment[k], str) else f'{comment[k]["firstname"]} {comment[k]["lastname"]}'
         for k in comment.keys() if k in allowed] for comment in comments]
    return filtered_comments


def activity_kudos(activity_id):
    kudos = activ.activity_kudos(activity_id=activity_id)
    if "errors" in kudos:
        return f"Invalid activity id: {activity_id}."
    if not kudos:
        return f"No kudos found for activity: {activity_id}."
    kudoers = [[kudo[k] for k in kudo.keys() if k != "resource_state"] for kudo in kudos]
    return kudoers
