from tabulate import tabulate


def activities_table(data: list):
    header = data[0].keys()
    rows = [x.values() for x in data]
    print("\n")
    print(tabulate(rows, header, tablefmt="pretty", showindex="always"))


def stats_tabe(data: list):
    print("\n")
    print(tabulate(data, tablefmt="pretty", showindex="always", numalign="left"))


def activity_table(data):
    if isinstance(data, list):
        print("\n")
        print(tabulate(data, tablefmt="simple_grid", numalign="left"))
    else:
        print(data)


def comments_table(data):
    if isinstance(data, list):
        print("\n")
        print(tabulate(data, ["Comment", "Author"], tablefmt="simple_grid", numalign="left"))
    else:
        print(data)


def kudos_table(data):
    if isinstance(data, list):
        print("\n")
        print(tabulate(data, headers=["Firstname", "Lastname"], tablefmt="simple_grid", numalign="left",
                       showindex="always"))
    else:
        print(data)
