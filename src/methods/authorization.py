import urllib3
import requests
import json
from src.methods import api_endpoints

# Disables harmless warning
urllib3.disable_warnings()


def access_token():
    # Loads crucial/sensitive parameters necessary for retriving actual data from cretentials.json file
    with open("credentials.json", "r") as f:
        data = f.read()
    credentials = json.loads(data)

    # Creating payload for authorization request
    payload: dict = {
        'grant_type': "refresh_token",
        'f': 'json'
    }
    payload.update(credentials)

    resp = requests.post(api_endpoints.auth, data=payload, verify=False)
    return resp.json()['access_token']

