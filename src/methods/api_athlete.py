import urllib3
import requests

from src.methods import api_endpoints
from src.methods.authorization import access_token

# Disables harmless warning
urllib3.disable_warnings()


class Athlete:
    def __init__(self):
        self.access_token = access_token()
        self.header = {"Authorization": f"Authorization: Bearer {self.access_token}"}

    def activities(self, page_items=30, page_index=1) -> list:
        """
        :param page_items: Numer of items on pafinated request result (max 200)
        :param page_index: Number of page
        :return: List of dictionaries
        """
        param = {'per_page': page_items, "page": page_index}
        activ = requests.get(f"{api_endpoints.athlete}/activities", headers=self.header, params=param).json()
        return activ

    def athlete(self) -> dict:
        athlete_info = requests.get(f"{api_endpoints.athlete}", headers=self.header).json()
        return athlete_info

    def stats(self, athlete_id: str) -> dict:
        athlete_statistics = requests.get(f"{api_endpoints.athlets}/{athlete_id}/stats", headers=self.header).json()
        return athlete_statistics

    def clubs(self) -> list:
        athlete_clubs = requests.get(f"{api_endpoints.athlete}/clubs", headers=self.header).json()
        return athlete_clubs


