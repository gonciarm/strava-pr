# Endpoints urls

# Authorization
auth = "https://www.strava.com/api/v3/oauth/token"

# Athlete
athlete = "https://www.strava.com/api/v3/athlete"
athlets = "https://www.strava.com/api/v3/athletes"

# Activities
activity = "https://www.strava.com/api/v3/activities"

# Clubs
clubs = "https://www.strava.com/api/v3/clubs"

