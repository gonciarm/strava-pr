import urllib3
import requests

from src.methods import api_endpoints
from src.methods.authorization import access_token

# Disables harmless warning
urllib3.disable_warnings()


class Activities:
    def __init__(self):
        self.access_token = access_token()
        self.header = {"Authorization": f"Authorization: Bearer {self.access_token}"}

    def activity(self, activity_id: int) -> dict:
        activity = requests.get(f"{api_endpoints.activity}/{activity_id}", headers=self.header).json()
        return activity

    def activity_comments(self, activity_id: int) -> list:
        comments = requests.get(f"{api_endpoints.activity}/{activity_id}/comments", headers=self.header).json()
        return comments

    def activity_kudos(self, activity_id: int) -> list:
        kudos = requests.get(f"{api_endpoints.activity}/{activity_id}/kudos", headers=self.header).json()
        return kudos

    def activity_laps(self, activity_id: int) -> list:
        laps = requests.get(f"{api_endpoints.activity}/{activity_id}/laps", headers=self.header).json()
        return laps

    def activity_zones(self, activity_id: int) -> list:
        zones = requests.get(f"{api_endpoints.activity}/{activity_id}/zones", headers=self.header).json()
        return zones
