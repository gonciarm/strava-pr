import urllib3
import requests

from src.methods import api_endpoints
from src.methods.authorization import access_token

# Disables harmless warning
urllib3.disable_warnings()


class Clubs:
    def __init__(self):
        self.access_token = access_token()
        self.header = {"Authorization": f"Authorization: Bearer {self.access_token}"}

    def club_activities(self, club_id: int, page_index=1) -> list:
        param = {'per_page': 1, "page": page_index}
        club_activ = requests.get(f"api_endpoints.clubs/{club_id}/activities", headers=self.header, params=param).json()
        return club_activ

    def club_administrators(self, club_id: int) -> list:
        club_admins = requests.get(f"api_endpoints.clubs/{club_id}/admins", headers=self.header).json()
        return club_admins

    def club(self, club_id: int) -> list:
        club = requests.get(f"api_endpoints.clubs/{club_id}", headers=self.header).json()
        return club

    def club_members(self, club_id: int, page_index=1) -> list:
        param = {'per_page': 1, "page": page_index}
        club_members = requests.get(f"api_endpoints.clubs/{club_id}/members", headers=self.header, params=param).json()
        return club_members

